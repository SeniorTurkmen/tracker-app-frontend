import 'package:dio/dio.dart';

class NetworkServices {
  Dio dio = Dio();
  static final String baseUrl = 'https://d8d04c0b60d5.ngrok.io';
  static final String authLogin = '$baseUrl/auth/login';
  static final String status = '$baseUrl/ogrenci/setStatus';

  Future<Map<String, dynamic>> login(
      String email, String password, String type) async {
    Map<String, dynamic> body = {
      'email': email ?? 'abc@zxczx.com',
      'password': password ?? '13224468',
      'type': type
    };
    Response res = await dio.post(authLogin, data: body);
    return res.data['user'];
  }

  Future<Map<String, dynamic>> setStatus(String title, String id) async {
    Map<String, dynamic> body = {
      'id': id,
      'status': title,
    };
    Response res = await dio.post(status, data: body);
    print(res.data);
    return res.data;
  }
}
