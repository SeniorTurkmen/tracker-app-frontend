import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tracker/components/layout/custom_bottom_bar/tab_item.dart';

class CustomBottomNavBar extends StatefulWidget {
  CustomBottomNavBar(
      {Key key,
      @required this.currentTab,
      @required this.onSelectedTab,
      @required this.pageBuilder,
      @required this.navigatorKeys,
      @required this.routes,
      @required this.items,
      @required this.tabItems})
      : super(key: key);

  final TabItem currentTab;
  final ValueChanged<TabItem> onSelectedTab;
  final Map<TabItem, Widget> pageBuilder;
  final Map<TabItem, GlobalKey<NavigatorState>> navigatorKeys;
  final Map<String, WidgetBuilder> routes;
  final List<BottomNavigationBarItem> items;
  final List<TabItem> tabItems;
  @override
  _CustomBottomNavBarState createState() => _CustomBottomNavBarState();
}

class _CustomBottomNavBarState extends State<CustomBottomNavBar> {
  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        iconSize: 30,
        activeColor: Colors.black,
        inactiveColor: Colors.black38,
        backgroundColor: Theme.of(context).canvasColor,
        items: widget.items,
        onTap: (index) => widget.onSelectedTab(TabItem.values[index]),
      ),
      tabBuilder: (context, index) {
        final showItem = widget.tabItems[index];
        return CupertinoTabView(
            routes: widget.routes,
            navigatorKey: widget.navigatorKeys[showItem],
            builder: (context) => widget.pageBuilder[showItem]);
      },
    );
  }
}
