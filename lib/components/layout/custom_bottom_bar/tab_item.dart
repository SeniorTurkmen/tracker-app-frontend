import 'package:flutter/material.dart';

enum TabItem {
  Home,
  NextStudent,
  DefinedStudent,
  StudentStatus,
  StudentInformation
}

class TabItemData {
  final String title;
  final IconData icon;

  TabItemData({this.icon, this.title});

  static Map<TabItem, TabItemData> allTabs = {
    TabItem.Home: TabItemData(icon: Icons.group, title: "Students"),
    TabItem.NextStudent:
        TabItemData(icon: Icons.next_plan, title: "Next Student"),
    TabItem.DefinedStudent:
        TabItemData(icon: Icons.verified, title: "Defined Student"),
  };
  static Map<TabItem, TabItemData> parentTabs = {
    TabItem.StudentStatus:
        TabItemData(icon: Icons.calendar_today, title: 'Students Status'),
    TabItem.StudentInformation: TabItemData(
        icon: Icons.perm_device_information, title: 'Student Information')
  };
}
