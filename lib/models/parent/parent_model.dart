import 'package:flutter/material.dart';
import 'package:tracker/models/data_model/ogrenci_model.dart';
import 'package:tracker/models/data_model/veli_model.dart';

enum ParentModelStatus {
  Ended,
  Loading,
  Error,
}

class ParentModel extends ChangeNotifier {
  ParentModelStatus _status;
  String _errorCode;
  String _errorMessage;

  String get errorCode => _errorCode;
  String get errorMessage => _errorMessage;
  ParentModelStatus get status => _status;
  Veli veli;
  Ogrenciler selectedOgrenci;
  Map<String, dynamic> selectedHistory;
  DateTime selectedDay;
  bool isReadySave = false;
  List<String> deliveryStatus = [];

  ParentModel();

  saveParentData(Veli data) {
    changeStatus(ParentModelStatus.Loading);
    print('hello');
    veli = data;
    print(data.toRawJson());
    selectedOgrenci = veli.ogrenciler[0];
    changeStatus(ParentModelStatus.Ended);
  }

  void setSelectedStudent(Ogrenciler ogrenci, {String history}) {
    selectedOgrenci = ogrenci;
    if (history != null) {
      setCurrentHistory(selectedDay);
    }
    changeStatus(ParentModelStatus.Ended);
  }

  void setCurrentHistory(DateTime day) {
    var isChanged = false;
    selectedOgrenci.history.forEach((element) {
      var dateTime = DateTime.parse(element['dateTime'].toString());
      if (dateTime.day == day.day &&
          dateTime.month == day.month &&
          dateTime.year == day.year) {
        selectedHistory = element;
        if (selectedHistory['deliveryStatus'] != null) {
          deliveryStatus = <String>[...selectedHistory['deliveryStatus']];
        }
        isChanged = true;
      }
    });
    if (!isChanged) {
      selectedHistory = null;
      deliveryStatus = [];
    }
    var list = [];
    changeStatus(ParentModelStatus.Ended);
  }

  void setSelectedDay(day) {
    selectedDay = day;
    changeStatus(ParentModelStatus.Ended);
  }

  changeStatus(ParentModelStatus stat) {
    _status = stat;
    notifyListeners();
  }

  void addDeliveryStatus(String s) {
    deliveryStatus.add(s);
    isReadySave = true;
    changeStatus(ParentModelStatus.Ended);
  }

  void removeDeliveryStatus(String s) {
    deliveryStatus.remove(s);
    isReadySave = true;
    changeStatus(ParentModelStatus.Ended);
  }
}
