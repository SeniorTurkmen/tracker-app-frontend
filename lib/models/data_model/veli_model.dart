// To parse this JSON data, do
//
//     final veli = veliFromJson(jsonString);

import 'dart:collection';
import 'dart:convert';

import 'package:tracker/models/data_model/ogrenci_model.dart';

class Veli {
  Veli({
    this.ogrenciler,
    this.id,
    this.name,
    this.surName,
    this.email,
    this.telefon,
    this.password,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  List<Ogrenciler> ogrenciler;
  String id;
  String name;
  String surName;
  String email;
  String telefon;
  String password;
  DateTime createdAt;
  DateTime updatedAt;
  int v;

  factory Veli.fromRawJson(String str) => Veli.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Veli.fromJson(Map<String, dynamic> json) => Veli(
        ogrenciler: List<Ogrenciler>.from(json["ogrenciler"].map((x) {
          print(x);
          return Ogrenciler.fromJson(x is String ? {} : x);
        })),
        id: json["_id"],
        name: json["name"],
        surName: json["surName"],
        email: json["email"],
        telefon: json["telefon"],
        password: json["password"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "ogrenciler": ogrenciler != null
            ? List<dynamic>.from(ogrenciler.map((x) => x.toJson()))
            : null,
        "_id": id,
        "name": name,
        "surName": surName,
        "email": email,
        "telefon": telefon,
        "password": password,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
      };

  @override
  String toString() {
    return '$ogrenciler';
  }
}
