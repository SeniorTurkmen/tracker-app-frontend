import 'dart:convert';

import 'adress_model.dart';

class Okul {
  Okul({
    this.id,
    this.email,
    this.telefon,
    this.name,
    this.surName,
    this.password,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.adress,
  });

  String id;
  String email;
  String telefon;
  String name;
  String surName;
  String password;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  Adress adress;

  factory Okul.fromRawJson(String str) => Okul.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Okul.fromJson(Map<String, dynamic> json) => Okul(
        id: json["_id"],
        email: json["email"],
        telefon: json["telefon"],
        name: json["name"],
        surName: json["surName"] == null ? null : json["surName"],
        v: json["__v"],
        adress: json["adress"] == null ? null : Adress.fromJson(json["adress"]),
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "email": email,
        "telefon": telefon,
        "name": name,
        "surName": surName == null ? null : surName,
        "__v": v,
        "adress": adress == null ? null : adress.toJson(),
      };
}
