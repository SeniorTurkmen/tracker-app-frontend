import 'dart:convert';

import 'adress_model.dart';
import 'okul_model.dart';
import 'veli_model.dart';

class Ogrenciler {
  Ogrenciler({
    this.id,
    this.name,
    this.surName,
    this.status,
    this.okul,
    this.adress,
    this.veli,
    this.history,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  String id;
  String name;
  String surName;
  String status;
  Okul okul;
  Adress adress;
  Veli veli;
  List<dynamic> history;
  DateTime createdAt;
  DateTime updatedAt;
  int v;

  factory Ogrenciler.fromRawJson(String str) =>
      Ogrenciler.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Ogrenciler.fromJson(Map<String, dynamic> json) => Ogrenciler(
        id: json["_id"],
        name: json["name"],
        surName: json["surName"],
        status: json["status"],
        okul: json["okul"] != null
            ? Okul.fromJson(json["okul"] is String ? {} : json["okul"])
            : null,
        adress: json["adress"] != null ? Adress.fromJson(json["adress"]) : null,
        veli: json["veli"] != null ? Veli.fromJson(json["veli"]) : null,
        history: json["history"],
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "name": name,
        "surName": surName,
        "status": status,
        "okul": okul.toJson(),
        "adress": adress.toJson(),
        "history": List<dynamic>.from(history.map((x) => x)),
        "__v": v,
      };
}
