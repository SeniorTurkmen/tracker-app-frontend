import 'dart:convert';

import 'ogrenci_model.dart';

class Surucu {
  Surucu({
    this.ogrenciler,
    this.id,
    this.email,
    this.telefon,
    this.name,
    this.surName,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  List<Ogrenciler> ogrenciler;
  String id;
  String email;
  String telefon;
  String name;
  String surName;
  DateTime createdAt;
  DateTime updatedAt;
  int v;

  factory Surucu.fromRawJson(String str) => Surucu.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Surucu.fromJson(Map<String, dynamic> json) => Surucu(
        ogrenciler: json["ogrenciler"] == null
            ? null
            : List<Ogrenciler>.from(
                json["ogrenciler"].map((x) => Ogrenciler.fromJson(x))),
        id: json["_id"],
        email: json["email"],
        telefon: json["telefon"],
        name: json["name"],
        surName: json["surName"] == null ? null : json["surName"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "ogrenciler": ogrenciler == null
            ? null
            : List<dynamic>.from(ogrenciler.map((x) => x.toJson())),
        "_id": id,
        "email": email,
        "telefon": telefon,
        "name": name,
        "surName": surName == null ? null : surName,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
      };
}
