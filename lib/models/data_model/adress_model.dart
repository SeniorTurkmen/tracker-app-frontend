import 'dart:convert';

class Adress {
  Adress({
    this.id,
    this.cad,
    this.mahalle,
    this.numara,
    this.il,
    this.ilce,
    this.longtidute,
    this.latitude,
    this.v,
  });

  String id;
  String cad;
  String mahalle;
  String numara;
  String il;
  String ilce;
  double longtidute;
  double latitude;
  int v;

  factory Adress.fromRawJson(String str) => Adress.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Adress.fromJson(Map<String, dynamic> json) => Adress(
        id: json["_id"],
        cad: json["cad"],
        mahalle: json["mahalle"],
        numara: json["numara"],
        il: json["il"],
        ilce: json["ilce"],
        longtidute: json["longtidute"].toDouble(),
        latitude: json["latitude"].toDouble(),
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "cad": cad,
        "mahalle": mahalle,
        "numara": numara,
        "il": il,
        "ilce": ilce,
        "longtidute": longtidute,
        "latitude": latitude,
        "__v": v,
      };
  @override
  String toString() {
    return 'Mahalle: $mahalle\nSok/Cad: $cad\nNo: $numara\nİl/İlçe: $il/$ilce';
  }
}
