import 'package:flutter/material.dart';
import 'package:tracker/components/services/network_service.dart';
import 'package:tracker/models/data_model/ogrenci_model.dart';
import 'package:tracker/models/data_model/surucu_model.dart';
import 'package:tracker/models/data_model/user_data_model.dart';

enum DriverHomeModelStatus {
  Ended,
  Loading,
  Error,
}

class DriverHomeModel extends ChangeNotifier {
  DriverHomeModelStatus status = DriverHomeModelStatus.Loading;
  String _errorCode;
  String _errorMessage;
  List<Student> _studentData;
  Ogrenciler _nextStudent;
  bool _isAnyStudentNext = false;
  bool checkIsEvryOneInCar = false;
  bool isSchoolArrived = false;
  Surucu surucu;

  String get errorCode => _errorCode;
  String get errorMessage => _errorMessage;
  List<Student> get studentData => _studentData;
  Ogrenciler get nextStudent => _nextStudent;
  bool get isAnyStudentNext => _isAnyStudentNext;

  NetworkServices service = NetworkServices();

  DriverHomeModel();

  void setNextStudent(String id) {
    status = DriverHomeModelStatus.Loading;
    notifyListeners();
    surucu.ogrenciler.firstWhere((element) => element.id == id).status =
        'next Student';
    sendStatus('next Student', id);
    _nextStudent = surucu.ogrenciler.firstWhere((element) => element.id == id);
    _isAnyStudentNext = true;
    checkEveryOne();
    status = DriverHomeModelStatus.Ended;
    notifyListeners();
  }

  void setStudentInVehicle(String id) {
    status = DriverHomeModelStatus.Loading;
    notifyListeners();
    surucu.ogrenciler.firstWhere((element) => element.id == id).status =
        'in Bus';
    sendStatus('in Bus', id);
    _nextStudent = null;
    _isAnyStudentNext = false;
    checkEveryOne();

    status = DriverHomeModelStatus.Ended;
    notifyListeners();
  }

  void sendStatus(String title, String id) {
    service.setStatus(title, id);
  }

  void setSurucu(Surucu val) {
    surucu = val;
    checkEveryOne();
    notifyListeners();
  }

  void checkEveryOne() {
    if (!(surucu.ogrenciler[0].status == 'on School Road')) {
      bool marknull = false;
      for (var item in surucu.ogrenciler) {
        if (item.status != 'in Bus') {
          marknull = true;
          break;
        }
      }
      if (!marknull) {
        checkIsEvryOneInCar = true;
      }
    } else {
      isSchoolArrived = true;
    }
  }

  void setSchoolRoad({String title = 'on School Road'}) {
    surucu.ogrenciler.forEach((element) {
      element.status = title;
      sendStatus(title, element.id);
    });
    checkEveryOne();
    notifyListeners();
  }
}
