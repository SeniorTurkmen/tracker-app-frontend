import 'package:flutter/material.dart';
import 'package:tracker/components/services/network_service.dart';
import 'package:tracker/models/data_model/surucu_model.dart';
import 'package:tracker/models/data_model/veli_model.dart';

enum AuthModelStatus {
  Ended,
  Loading,
  Error,
}

class AuthModel extends ChangeNotifier {
  AuthModelStatus _status;
  String _errorCode;
  String _errorMessage;

  String get errorCode => _errorCode;
  String get errorMessage => _errorMessage;
  AuthModelStatus get status => _status;
  Veli veli;
  Surucu surucu;

  AuthModel();
  NetworkServices conn = NetworkServices();

  Future login(String email, String pass, String type) async {
    try {
      _errorMessage = null;
      print('hi');
      changeStatus(AuthModelStatus.Loading);
      var res = await conn.login(email, pass, type);
      print(res);
      switch (type.toLowerCase()) {
        case 'okul':
          break;
        case 'veli':
          veli = Veli.fromJson(res);
          changeStatus(AuthModelStatus.Ended);

          return veli;
          break;
        case 'surucu':
          print('hello0');
          surucu = Surucu.fromJson(res);
          changeStatus(AuthModelStatus.Ended);
          return surucu;
          break;
        default:
          throw Exception('unexpected Exception');
      }
      changeStatus(AuthModelStatus.Ended);
    } catch (e, stacktrace) {
      print(e);
      print(stacktrace);
      changeStatus(AuthModelStatus.Error);
      throw Exception();
    }
  }

  changeStatus(AuthModelStatus stat) {
    _status = stat;
    notifyListeners();
  }
}
