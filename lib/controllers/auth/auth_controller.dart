import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// import model
import 'package:tracker/models/auth/auth_model.dart';
import 'package:tracker/models/driver_home/driver_home_model.dart';
import 'package:tracker/models/parent/parent_model.dart';

class AuthController {
  Future login(
      BuildContext context, String email, String pass, String type) async {
    AuthModel viewModel = Provider.of<AuthModel>(context, listen: false);
    ParentModel parentModel = Provider.of<ParentModel>(context, listen: false);
    DriverHomeModel driverModel =
        Provider.of<DriverHomeModel>(context, listen: false);
    var data = await viewModel.login(email, pass, type);
    try {
      switch (type.toLowerCase()) {
        case 'okul':
          break;
        case 'veli':
          print(data);
          parentModel.saveParentData(data);
          break;
        case 'surucu':
          driverModel.setSurucu(data);
          break;
        default:
          throw Exception('unexpected Exception');
      }
    } catch (e) {
      print(e);
      throw Exception();
    }
  }
}
