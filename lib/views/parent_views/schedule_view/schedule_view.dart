import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tracker/components/layout/appbar.dart';
import 'package:tracker/components/util/screen_util.dart';
import 'package:tracker/models/parent/parent_model.dart';

class ScheduleView extends StatelessWidget {
  static String routeName = '/Schedule-Screen';
  @override
  Widget build(BuildContext context) {
    return Consumer<ParentModel>(
      builder: (ctx, parent, child) => WillPopScope(
        onWillPop: () async {
          parent.isReadySave = false;
          return true;
        },
        child: Scaffold(
          appBar: getAppBar(ctx: context, title: 'Schedule Transport'),
          floatingActionButton: parent.isReadySave
              ? FloatingActionButton(
                  onPressed: () {},
                  child: Icon(
                    Icons.save,
                    color: Colors.black,
                  ),
                  backgroundColor: Theme.of(context).primaryColor,
                )
              : null,
          body: Container(
            color: Colors.white,
            height: double.infinity,
            alignment: Alignment.bottomCenter,
            child: Container(
              height: SizeConfig.heightMultiplier * 75,
              width: SizeConfig.widthMultiplier * 100,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(SizeConfig.widthMultiplier * 10),
                      topRight:
                          Radius.circular(SizeConfig.widthMultiplier * 10)),
                  color: Colors.grey.withOpacity(.1)),
              child: Container(
                  margin: EdgeInsets.symmetric(
                      vertical: SizeConfig.heightMultiplier * 6,
                      horizontal: SizeConfig.widthMultiplier * 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(formatDate(
                          parent.selectedDay, [dd, ' ', M, ' ', yyyy])),
                      SizedBox(
                        height: 30,
                      ),
                      if (parent.veli.ogrenciler.length > 1)
                        _studentTabs(parent),
                      Text(
                        '${parent.selectedOgrenci.name} ${parent.selectedOgrenci.surName}',
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 18),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 30),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Öğrencinin Durumu:',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 18),
                            ),
                            Text(
                              '${parent.selectedOgrenci.status}',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal, fontSize: 18),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 30),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Teslim Durumu:',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18),
                                ),
                                TextButton(
                                  onPressed: () {
                                    showModalBottomSheet(
                                        context: context,
                                        builder: (context) {
                                          return StatefulBuilder(
                                            builder: (ctx, setState) => Column(
                                              mainAxisSize: MainAxisSize.min,
                                              children: <Widget>[
                                                CheckboxListTile(
                                                    title: Text('Okuldan Eve'),
                                                    value: parent.deliveryStatus
                                                        .contains(
                                                            'Okuldan Eve'),
                                                    onChanged: (val) {
                                                      if (val) {
                                                        parent
                                                            .addDeliveryStatus(
                                                                'Okuldan Eve');
                                                      } else {
                                                        parent
                                                            .removeDeliveryStatus(
                                                                'Okuldan Eve');
                                                      }
                                                      setState(() {});
                                                    }),
                                                CheckboxListTile(
                                                    title: Text('Evden okula'),
                                                    value: parent.deliveryStatus
                                                        .contains(
                                                            'Evden okula'),
                                                    onChanged: (val) {
                                                      if (val) {
                                                        parent
                                                            .addDeliveryStatus(
                                                                'Evden okula');
                                                      } else {
                                                        parent
                                                            .removeDeliveryStatus(
                                                                'Evden okula');
                                                      }
                                                      setState(() {});
                                                    }),
                                              ],
                                            ),
                                          );
                                        });
                                  },
                                  child: Text(
                                    'düzenle',
                                    style: TextStyle(
                                        decoration: TextDecoration.underline,
                                        color: Theme.of(context).primaryColor,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 15),
                                  ),
                                )
                              ],
                            ),
                            if (parent.selectedHistory != null)
                              parent.selectedHistory['deliveryStatus']
                                  .map((e) => Text(
                                        '${parent.selectedOgrenci.status}',
                                        style: TextStyle(
                                            fontWeight: FontWeight.normal,
                                            fontSize: 18),
                                      ))
                                  .toList(),
                            if (parent.selectedHistory == null)
                              ...parent.deliveryStatus
                                  .map((e) => Text(
                                        '$e',
                                        style: TextStyle(
                                            fontWeight: FontWeight.normal,
                                            fontSize: 18),
                                      ))
                                  .toList(),
                          ],
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        ),
      ),
    );
  }

  Row _studentTabs(ParentModel parent) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        ...parent.veli.ogrenciler.map((e) => TextButton(
            onPressed: () => parent.setSelectedStudent(e),
            child: Text('${e.name} ${e.surName}')))
      ],
    );
  }
}
