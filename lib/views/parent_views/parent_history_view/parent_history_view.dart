import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timelines/timelines.dart';
import 'package:tracker/components/layout/appbar.dart';
import 'package:tracker/models/parent/parent_model.dart';

class HistoryView extends StatelessWidget {
  static String routeName = '/Student-History';
  const HistoryView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ParentModel>(
        builder: (ctx, parent, child) => Scaffold(
              appBar: getAppBar(ctx: ctx, title: 'Student History Detail'),
              body: Column(
                children: [
                  Text(formatDate(parent.selectedDay, [dd, ' ', M, ' ', yyyy])),
                  if (parent.veli.ogrenciler.length > 1)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        ...parent.veli.ogrenciler.map((e) => TextButton(
                            onPressed: () =>
                                parent.setSelectedStudent(e, history: 'doit'),
                            child: Text('${e.name} ${e.surName}')))
                      ],
                    ),
                  if (parent.selectedHistory != null)
                    ...{
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              parent.selectedHistory['status'].last['title'],
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Text(
                              formatDate(
                                  DateTime.parse(parent
                                      .selectedHistory['status'].last['time']
                                      .toString()),
                                  [dd, ' ', M, ' ', yyyy, ' - ', HH, ':', nn]),
                              style: TextStyle(fontWeight: FontWeight.w100),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 25),
                        child: timeLineWidget(parent),
                      )
                    }.toList(),
                  if (parent.selectedHistory == null)
                    Expanded(
                      child: Center(
                        child:
                            Text('we can\'t find any activity about this day '),
                      ),
                    )
                ],
              ),
            ));
  }

  Timeline timeLineWidget(ParentModel parent) {
    return Timeline(
      shrinkWrap: true,
      children: [
        ...parent.selectedHistory['status']
            .map((e) => _timeLineNodeTile(e))
            .toList()
      ],
    );
  }

  TimelineTile _timeLineNodeTile(e) {
    return TimelineTile(
      nodeAlign: TimelineNodeAlign.start,
      contents: Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              e['title'],
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(
              formatDate(DateTime.parse(e['time'].toString()),
                  [yyyy, ' ', mm, ' ', dd, ' - ', HH, ':', nn]),
              style: TextStyle(fontWeight: FontWeight.w100),
            )
          ],
        ),
      )),
      node: TimelineNode(
        indicator: DotIndicator(),
        startConnector: SolidLineConnector(),
        endConnector: SolidLineConnector(),
      ),
    );
  }
}
