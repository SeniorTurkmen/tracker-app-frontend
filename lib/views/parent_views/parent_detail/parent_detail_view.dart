import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tracker/components/util/screen_util.dart';
import 'package:tracker/models/parent/parent_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ParentDetailView extends StatelessWidget {
  static const routeName = "/parent-detail";
  @override
  Widget build(BuildContext context) {
    return Consumer<ParentModel>(
      builder: (ctx, parent, child) {
        return Scaffold(
            appBar: AppBar(
              title: Text("Account Detail"),
              backgroundColor: Colors.white,
              elevation: 0,
            ),
            // Behind Body Section
            body: Container(
                color: Colors.white,
                height: double.infinity,
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: SizeConfig.heightMultiplier * 75,
                  width: SizeConfig.widthMultiplier * 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft:
                              Radius.circular(SizeConfig.widthMultiplier * 10),
                          topRight:
                              Radius.circular(SizeConfig.widthMultiplier * 10)),
                      color: Colors.grey.withOpacity(.1)),
                  child: Container(
                    margin: EdgeInsets.symmetric(
                        vertical: SizeConfig.heightMultiplier * 6,
                        horizontal: SizeConfig.widthMultiplier * 5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        // Parent Name Section
                        Text(
                          'Veli',
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 18),
                        ),
                        _parentNameSection(
                            '${parent.veli.name} ${parent.veli.surName}'),
                        SizedBox(
                          height: 25,
                        ),
                        if (parent.veli.ogrenciler.length > 1)
                          _studentTabs(parent),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Ögrenci',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 18),
                                    ),
                                    Text(
                                      '${parent.selectedOgrenci.name} ${parent.selectedOgrenci.surName}',
                                    ),
                                    Text(
                                      '${parent.selectedOgrenci.status}',
                                    ),
                                    Text(
                                      '${parent.selectedOgrenci.adress}',
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: GoogleMap(
                                  markers: <Marker>[
                                    Marker(
                                        markerId: MarkerId(
                                            '${parent.selectedOgrenci.name}Marker'),
                                        position: LatLng(
                                            parent.selectedOgrenci.adress
                                                .latitude,
                                            parent.selectedOgrenci.adress
                                                .longtidute))
                                  ].toSet(),
                                  initialCameraPosition: CameraPosition(
                                      zoom: 13,
                                      target: LatLng(
                                          parent
                                              .selectedOgrenci.adress.latitude,
                                          parent.selectedOgrenci.adress
                                              .longtidute)),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                )));
      },
    );
  }

  Row _studentTabs(ParentModel parent) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        ...parent.veli.ogrenciler.map((e) => TextButton(
            onPressed: () => parent.setSelectedStudent(e),
            child: Text('${e.name} ${e.surName}')))
      ],
    );
  }

  Text _parentNameSection(String val) {
    return Text(
      val,
      style: TextStyle(
          fontSize: SizeConfig.textMultiplier * 3, fontWeight: FontWeight.w600),
    );
  }
}
