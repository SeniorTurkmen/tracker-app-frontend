import 'package:flutter/material.dart';
import 'package:tracker/components/layout/custom_bottom_bar/custom_bottom_nav_bar.dart';
import 'package:tracker/components/layout/custom_bottom_bar/tab_item.dart';
import 'package:tracker/views/parent_views/parent_detail/parent_detail_view.dart';
import 'package:tracker/views/parent_views/parent_history_view/parent_history_view.dart';
import 'package:tracker/views/parent_views/parent_home/parent_home.dart';
import 'package:tracker/views/parent_views/schedule_view/schedule_view.dart';

class ParentTabs extends StatefulWidget {
  ParentTabs({Key key}) : super(key: key);

  @override
  _ParentTabsState createState() => _ParentTabsState();
}

class _ParentTabsState extends State<ParentTabs> {
  TabItem _currentTab = TabItem.StudentStatus;

  Map<TabItem, GlobalKey<NavigatorState>> navigatorKeys = {
    TabItem.StudentInformation: GlobalKey<NavigatorState>(),
    TabItem.StudentStatus: GlobalKey<NavigatorState>(),
  };

  Map<TabItem, Widget> allPages() {
    return {
      TabItem.StudentInformation: ParentDetailView(),
      TabItem.StudentStatus: ParentHome(),
    };
  }

  List<TabItem> tabItems = [
    TabItem.StudentStatus,
    TabItem.StudentInformation,
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async =>
          !await navigatorKeys[_currentTab].currentState.maybePop(),
      child: CustomBottomNavBar(
        tabItems: tabItems,
        items: [
          _createNavItem(TabItem.StudentStatus),
          _createNavItem(TabItem.StudentInformation),
        ],
        routes: {
          '/': (ctx) => ParentTabs(),
          ParentHome.routeName: (ctx) => ParentHome(),
          HistoryView.routeName: (ctx) => HistoryView(),
          ParentDetailView.routeName: (ctx) => ParentDetailView(),
          ScheduleView.routeName: (ctx) => ScheduleView()
        },
        pageBuilder: allPages(),
        navigatorKeys: navigatorKeys,
        currentTab: _currentTab,
        onSelectedTab: (selectedTab) {
          if (selectedTab == _currentTab) {
            navigatorKeys[selectedTab]
                .currentState
                .popUntil((route) => route.isFirst);
          } else {
            setState(() {
              _currentTab = selectedTab;
            });
          }
        },
      ),
    );
  }

  BottomNavigationBarItem _createNavItem(TabItem tabItem) {
    final tabToCreate = TabItemData.parentTabs[tabItem];
    return BottomNavigationBarItem(
        label: tabToCreate.title, icon: Icon(tabToCreate.icon));
  }
}
