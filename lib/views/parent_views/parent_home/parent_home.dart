import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:tracker/components/layout/appbar.dart';
import 'package:tracker/models/data_model/event_model.dart';
import 'package:tracker/models/parent/parent_model.dart';
import 'package:tracker/views/parent_views/parent_history_view/parent_history_view.dart';
import 'package:tracker/views/parent_views/schedule_view/schedule_view.dart';

class ParentHome extends StatelessWidget {
  static String routeName = '/Parent-Status';

  DateTime calculateDateTime(type) {
    var date = DateTime.now();
    return type == 'before'
        ? date.subtract(Duration(days: 365))
        : date.add(Duration(days: 365));
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ParentModel>(
        builder: (ctx, parent, child) => Scaffold(
            appBar: getAppBar(title: 'Student Status', ctx: context),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  TableCalendar(
                    onHeaderTapped: null,
                    headerStyle: HeaderStyle(
                      formatButtonVisible: false,
                      titleCentered: true,
                    ),
                    onDaySelected: (day, _) {
                      var dt = DateTime.now();
                      parent.setSelectedDay(day);
                      parent.setCurrentHistory(day);
                      if (day.day <= dt.day && day.month <= dt.month) {
                        Navigator.pushNamed(context, HistoryView.routeName);
                      } else {
                        Navigator.pushNamed(context, ScheduleView.routeName);
                      }
                    },
                    eventLoader: (day) => getEvents(parent, day),
                    focusedDay: DateTime.now(),
                    firstDay: calculateDateTime('before'),
                    lastDay: calculateDateTime('last'),
                    weekendDays: [6, 7],
                    startingDayOfWeek: StartingDayOfWeek.monday,
                  ),
                ],
              ),
            )));
  }

  List<Event> getEvents(ParentModel parent, DateTime day) {
    var events = <Event>[];
    parent.veli.ogrenciler.forEach((element) {
      element.history.forEach((element) {
        var tempDate = DateTime.parse(element['dateTime'].toString());
        if (tempDate.day == day.day && tempDate.month == day.month) {
          events.add(Event('schedule'));
        }
      });
    });
    return events;
  }
}
