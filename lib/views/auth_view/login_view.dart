import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:tracker/components/util/color_util.dart';
import 'package:tracker/components/util/screen_util.dart';
import 'package:tracker/controllers/auth/auth_controller.dart';
import 'package:tracker/models/auth/auth_model.dart';

class LoginView extends StatefulWidget {
  final Function(String, bool) callback;
  const LoginView({
    Key key,
    this.callback,
  }) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  var controller = AuthController();
  bool isLoading = false;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  String dropdownValue = 'Okul';

  final FocusNode focusNodeEmail = FocusNode();
  final FocusNode focusNodePassword = FocusNode();

  bool _obscureTextPassword = true;
  @override
  void initState() {
    super.initState();
    emailController = TextEditingController();
    passwordController = TextEditingController();
  }

  @override
  void dispose() {
    focusNodeEmail.dispose();
    focusNodePassword.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AuthModel>(
      builder: (ctx, auth, child) => _loginScreen(auth),
    );
  }

  Scaffold _loginScreen(AuthModel auth) {
    var height2 = SizeConfig.heightMultiplier * 50;
    var width2 = SizeConfig.widthMultiplier * 75;
    return Scaffold(
        backgroundColor: HexColor(hexColor: "FFD64D"),
        body: Center(
          child:
              SizedBox(height: height2, width: width2, child: _loginCard(auth)),
        ));
  }

  Card _loginCard(AuthModel auth) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius:
              BorderRadius.circular(SizeConfig.imagesSizeMultiplier * 3)),
      child: auth.status == AuthModelStatus.Loading
          ? SizedBox(
              width: SizeConfig.widthMultiplier * 3,
              height: SizeConfig.widthMultiplier * 3,
              child: Center(child: CircularProgressIndicator()))
          : Padding(
              padding: EdgeInsets.all(SizeConfig.widthMultiplier * 3),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  _emailSection(),
                  _passwordSection(),
                  _loginTypeSection(),
                  _loginButton()
                ],
              ),
            ),
    );
  }

  ElevatedButton _loginButton() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(primary: HexColor(hexColor: "000000")),
      onPressed: () async {
        try {
          await controller.login(context, emailController.text,
              passwordController.text, dropdownValue);
          widget.callback(dropdownValue.toLowerCase(), true);
        } catch (e) {
          print(e);
          widget.callback(dropdownValue.toLowerCase(), false);
        }
      },
      child: Text('Login'),
    );
  }

  TextField _passwordSection() {
    return TextField(
      focusNode: focusNodePassword,
      controller: passwordController,
      obscureText: _obscureTextPassword,
      style: const TextStyle(
          fontFamily: 'WorkSansSemiBold', fontSize: 16.0, color: Colors.black),
      decoration: InputDecoration(
        border: InputBorder.none,
        icon: const Icon(
          FontAwesomeIcons.lock,
          size: 22.0,
          color: Colors.black,
        ),
        hintText: 'Password',
        hintStyle:
            const TextStyle(fontFamily: 'WorkSansSemiBold', fontSize: 17.0),
        suffixIcon: GestureDetector(
          onTap: _toggleLogin,
          child: Icon(
            _obscureTextPassword
                ? FontAwesomeIcons.eye
                : FontAwesomeIcons.eyeSlash,
            size: 15.0,
            color: Colors.black,
          ),
        ),
      ),
      onSubmitted: (_) {},
      textInputAction: TextInputAction.go,
    );
  }

  TextField _emailSection() {
    return TextField(
      focusNode: focusNodeEmail,
      controller: emailController,
      keyboardType: TextInputType.emailAddress,
      style: const TextStyle(
          fontFamily: 'WorkSansSemiBold', fontSize: 16.0, color: Colors.black),
      decoration: const InputDecoration(
        border: InputBorder.none,
        icon: Icon(
          FontAwesomeIcons.envelope,
          color: Colors.black,
          size: 22.0,
        ),
        hintText: 'Email Address',
        hintStyle: TextStyle(fontFamily: 'WorkSansSemiBold', fontSize: 17.0),
      ),
      onSubmitted: (_) {
        focusNodePassword.requestFocus();
      },
    );
  }

  Row _loginTypeSection() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text('Choose Login Type:'),
        DropdownButton<String>(
          value: dropdownValue,
          icon: const Icon(Icons.arrow_downward),
          iconSize: 24,
          elevation: 16,
          underline: Container(
            color: Colors.transparent,
          ),
          onChanged: (String newValue) {
            setState(() {
              dropdownValue = newValue;
            });
          },
          items: <String>['Okul', 'Surucu', 'Veli']
              .map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
      ],
    );
  }

  void _toggleLogin() {
    setState(() {
      _obscureTextPassword = !_obscureTextPassword;
    });
  }
}
