import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tracker/views/driver_view/driver_tabs/driver_tabs.dart';
import 'package:tracker/views/parent_views/parent_tabs/parent_tabs.dart';
import 'package:tracker/views/widget/loading_widget.dart';

import 'login_view.dart';

class AuthMiddleware extends StatefulWidget {
  AuthMiddleware({Key key}) : super(key: key);

  @override
  _AuthMiddlewareState createState() => _AuthMiddlewareState();
}

class _AuthMiddlewareState extends State<AuthMiddleware> {
  static bool isSigned = false;
  static String type = '';
  changeStatus(_type, status) {
    type = _type;
    isSigned = status;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
    if (!isSigned)
      return LoginView(callback: changeStatus);
    else if (isSigned && type == 'okul')
      return Scaffold(
        appBar: AppBar(
          actions: [
            TextButton(
                onPressed: () => changeStatus('', false), child: Text('Exit'))
          ],
        ),
      );
    else if (isSigned && type == 'surucu')
      return DriverTabs();
    else if (isSigned && type == 'veli')
      return ParentTabs();
    else
      return LoadingWidget();
  }
}
